local PORT_NAV = 0xbee
local PORT_BROADCAST = 0xdbe

print("Starting...")    

local component = require "component"
local modem = component.modem
local computer = require("computer")
local os = require("os")

print("Awaiting drone")

modem.open(PORT_BROADCAST)

local function findDrone()
    while true do
        local evt,_,sender,port,dist,msg = computer.pullSignal()
        print("evt = " .. evt .. " msg = " .. (msg or "none"))
        if evt == "interrupted" then
            os.exit()
        end
        if evt == "modem_message" and port == PORT_BROADCAST and msg == "DRONE_RUNNING" then
            modem.send(sender,PORT_NAV,"CONTROL_REQUEST")
            return sender
        end
    end
end

local function controlDrone(drone) 
    while true do
        msg = io.read()
        if msg == "exit" then
            os.exit()
        end
        print("Sending " .. msg .. " to drone")
        modem.send(drone, PORT_NAV, msg)
    end
end

drone = findDrone()

print("Found!")

controlDrone(drone)



