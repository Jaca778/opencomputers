-- DRONE CODE

local PORT_NAV = 0xbee
local PORT_BROADCAST = 0xdbe

local drone = component.proxy(component.list("drone")())
local modem = component.proxy(component.list("modem")())

drone.move(0,1,0)
drone.setLightColor(0xFF0000)

modem.setStrength(20);
modem.broadcast(PORT_BROADCAST, "DRONE_RUNNING")

modem.open(PORT_NAV)

function awaitServer()
    while true do
        local evt,_,sender,port,dist,msg = computer.pullSignal()
        if evt == "modem_message" and port == PORT_NAV and msg == "CONTROL_REQUEST" then
            drone.setStatusText("Linked: "..sender:sub(1,3))
            modem.send(sender,port,"OK")
            return sender
        end
    end
end

function awaitInstructions(srv) 
    while true do
        local evt,_,sender,port,dist,msg = computer.pullSignal()
        drone.setStatusText(msg)
        if evt == "modem_message" and port == PORT_NAV and sender == srv then
            drone.setLightColor(0x0000FF)
            if msg == "FV" then 
                drone.move(1, 1, 0)
            elseif msg == "BV" then 
                drone.move(-1, 1, 0)
            end
            drone.setLightColor(0x00FF00)
        end
    end
end

local srv = awaitServer()

drone.setLightColor(0x00FF00)

awaitInstructions(srv)



